﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace zemlyaKormilitsa
{
	[Activity (Label = "APlants")]			
	public class APlants : Activity
	{
		protected override void OnCreate (Bundle bundle)
		{
			RequestWindowFeature (WindowFeatures.NoTitle);
			base.OnCreate (bundle);

			SetContentView (Resource.Layout.LPlants);

			Button button1 = FindViewById<Button> (Resource.Id.button1);
			Button button2 = FindViewById<Button> (Resource.Id.button2);
			Button button3 = FindViewById<Button> (Resource.Id.button3);
			Button button4 = FindViewById<Button> (Resource.Id.button4);
			Button button5 = FindViewById<Button> (Resource.Id.button5);
			Button button6 = FindViewById<Button> (Resource.Id.button6);
			Button button7 = FindViewById<Button> (Resource.Id.button7);
			Button button8 = FindViewById<Button> (Resource.Id.button8);



			button1.Click += delegate {
				Intent intent = new Intent (this,typeof(AInformationInOneActivity));

				intent.PutExtra ("table", "culture_plants" );

				StartActivity (intent);

			};

			button2.Click += delegate {
				Intent intent = new Intent (this,typeof(AInformationInOneActivity));

				intent.PutExtra ("table", "button1" );

				StartActivity (intent);

			};

			button3.Click += delegate {
				Intent intent = new Intent (this,typeof(AInformationInOneActivity));

				intent.PutExtra ("table", "button1" );

				StartActivity (intent);

			};

			button4.Click += delegate {
				Intent intent = new Intent (this,typeof(AInformationInOneActivity));

				intent.PutExtra ("table", "button1" );

				StartActivity (intent);

			};

			button5.Click += delegate {
				Intent intent = new Intent (this,typeof(AInformationInOneActivity));

				intent.PutExtra ("table", "button1" );

				StartActivity (intent);

			};

			button6.Click += delegate {
				Intent intent = new Intent (this,typeof(AInformationInOneActivity));

				intent.PutExtra ("table", "button1" );

				StartActivity (intent);

			};

			button7.Click += delegate {
				Intent intent = new Intent (this,typeof(AInformationInOneActivity));

				intent.PutExtra ("table", "button1" );

				StartActivity (intent);

			};

			button8.Click += delegate {
				Intent intent = new Intent (this,typeof(AInformationInOneActivity));

				intent.PutExtra ("table", "button1" );

				StartActivity (intent);

			};

			// Create your application here
		}
	}
}

