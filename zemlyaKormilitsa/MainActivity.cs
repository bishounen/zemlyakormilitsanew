﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace zemlyaKormilitsa
{
	[Activity (Label = "zemlyaKormilitsa", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : Activity
	{

		protected override void OnCreate (Bundle bundle)
		{
			RequestWindowFeature(WindowFeatures.NoTitle);


			base.OnCreate (bundle);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);

			// Get our button from the layout resource,
			// and attach an event to it
			Button button1 = FindViewById<Button> (Resource.Id.button1);
			Button button2 = FindViewById<Button> (Resource.Id.button2);
			Button button3 = FindViewById<Button> (Resource.Id.button3);
			Button button4 = FindViewById<Button> (Resource.Id.button4);
			Button button5 = FindViewById<Button> (Resource.Id.button5);
			Button button6 = FindViewById<Button> (Resource.Id.button6);
			Button button7 = FindViewById<Button> (Resource.Id.button7);


			button1.Click += delegate {
				Intent intent = new Intent (this,typeof(ATestingTheSoil));



				StartActivity (intent);

			};

			button2.Click += delegate {

			};

			button3.Click += delegate {
				Intent intent = new Intent (this,typeof(APlants));



				StartActivity (intent);
			};

			button4.Click += delegate {

			};

			button5.Click += delegate {

			};

			button6.Click += delegate {

			};

			button7.Click += delegate {

			};
		}
	}
}


